class Potepan::StaticPagesController < ApplicationController
  MAX_NUMBER_OF_NEW_PRODUCTS = 6

  def index
    @new_products = Spree::Product.
      where('available_on > ?', Time.zone.now.beginning_of_month). # 当月のリリース商品
      includes(master: [:default_price, :images]).
      order(Spree::Product.arel_table[:available_on].desc).
      limit(MAX_NUMBER_OF_NEW_PRODUCTS)
    @taxon_shirts = Spree::Taxon.find_by(name: "Shirts")
    @taxon_bags = Spree::Taxon.find_by(name: "Bags")
    @taxon_denim = Spree::Taxon.find_by(name: "denim")
  end
end
