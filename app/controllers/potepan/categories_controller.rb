class Potepan::CategoriesController < ApplicationController
  before_action :option_params, only: :show

  def show
    @taxonomies = Spree::Taxonomy.all.includes(:root)
    @taxon = Spree::Taxon.find(params[:id])
    @products = @taxon.all_products.includes(master: [:default_price, :images])
    @colors = find_option_type('Color')
    @sizes = find_option_type('Size')
    @option_value_color = Spree::OptionValue.find_by(id: @color)
    @option_value_size = Spree::OptionValue.find_by(id: @size)

    if params[:color] || params[:size]
      option_values_variant = Spree::OptionValuesVariant.
        select(:variant_id).
        where("option_value_id IN (?)", (@option_value_color || @option_value_size).id).
        distinct

      @products = @products.
        joins(:variants).
        where(spree_option_values_variants: { variant_id: option_values_variant }).
        includes(:variants, master: [:images, :default_price]).
        eager_load(variants: [:option_values_variants, :prices]).
        order(sort_products(@keyword)).
        distinct
    else
      @products = @products.
        eager_load(variants: [:option_values_variants, :prices]).
        order(sort_products(@keyword))
    end
  end

  private

  def option_params
    @display_type = params[:display_type] == 'list' ? 'list' : 'grid'
    @color        = params[:color]
    @size         = params[:size]
    @keyword      = params[:sort]
  end

  def find_option_type(name)
    if option_type = Spree::OptionType.find_by(name: name) # rubocop:disable Metrics/LineLength, Lint/AssignmentInCondition
      option_type.option_values.includes(:variants)
    end
  end

  def sort_products(keyword)
    case keyword
    when 'new'
      'spree_products.available_on DESC'
    when 'old'
      'spree_products.available_on ASC'
    when 'low'
      'default_prices_spree_variants.amount ASC'
    when 'high'
      'default_prices_spree_variants.amount DESC'
    else
      'spree_products.available_on DESC'
    end
  end
end
