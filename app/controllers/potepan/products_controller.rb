class Potepan::ProductsController < ApplicationController
  MAX_NUMBER_OF_PRODUCT_IMAGES = 4
  MAX_NUMBER_OF_RELATED_PRODUCTS = 4

  def show
    @product = Spree::Product.available.friendly.find(params[:id])
    @variants = @product.variants_including_master if @product.has_variants?
    @images = @product.variant_images.limit(MAX_NUMBER_OF_PRODUCT_IMAGES)
    taxons = @product.taxons
    if taxons.any?
      @related_products = @product.related_products(taxons).limit(MAX_NUMBER_OF_RELATED_PRODUCTS)
      @taxon = taxons.first
    end
  end

  def search
    @search_word = params[:search]
    @products = Spree::Product.
      search_products(@search_word).
      includes(master: [:default_price, :images])
  end
end
