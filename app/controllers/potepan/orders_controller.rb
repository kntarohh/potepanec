class Potepan::OrdersController < ApplicationController
  include Spree::Core::ControllerHelpers::Order
  include Spree::Core::ControllerHelpers::Store
  include Spree::Core::ControllerHelpers::Auth

  # before_action :check_authorization # orders_specの通過用にコメントアウト
  before_action :assign_order, only: :update
  before_action :get_order, only: :edit
  helper 'spree/products', 'spree/orders'
  respond_to :html

  # Shows the current incomplete order from the session
  def edit
  end

  def update
    authorize! :update, @order

    @order.contents.update_cart(order_params)
    redirect_to potepan_cart_url
  end

  def populate
    @order = current_order(create_order_if_necessary: true)

    variant  = Spree::Variant.find(params[:order][:variant_id])
    quantity = params[:order][:quantity].present? ? params[:order][:quantity].to_i : 1

    @line_item = @order.contents.add(variant, quantity)
    redirect_to potepan_cart_url
  end

  def remove_line_item
    @order = current_order
    line_item = @order.line_items.find(params[:line_item])
    @order.contents.remove_line_item(line_item)
    redirect_to potepan_cart_url
  end

  def delivery_address
  end

  def payment
  end

  def confirm
  end

  def checkout_complete
  end

  private

  def check_authorization
    order = Spree::Order.find_by(number: params[:id]) if params[:id].present?
    order ||= current_order

    if order && action_name.to_sym == :show
      authorize! :show, order, cookies.signed[:token]
    elsif order
      authorize! :edit, order, cookies.signed[:token]
    else
      authorize! :create, Spree::Order
    end
  end

  def order_params
    params.require(:order).permit(line_items_attributes: [:quantity, :id]) # variant_id → id に変更。idはline_itemのidのこと。variant_idを使うとアップデートに失敗し、追加処理になってしまう。 # rubocop:disable Metrics/LineLength
  end

  def assign_order
    @order = current_order
    unless @order
      flash[:error] = Spree.t(:order_not_found)
      redirect_to potepan_url && return
    end
  end

  def get_order
    @order = current_order
    @line_items = @order.line_items.includes(:product, :variant)
  end
end
