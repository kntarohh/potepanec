module ApplicationHelper
  def full_title(page_title = '') # rubocop:disable Airbnb/OptArgParameters
    base_title = "Potepanec"
    if page_title.blank?
      base_title
    else
      "#{page_title} | #{base_title}"
    end
  end
end
