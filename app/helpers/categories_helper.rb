module CategoriesHelper
  def variants_count_on_option(option:, taxon:)
    Spree::Variant.
      select(:product_id).
      joins(:option_values).
      where(product_id: taxon.all_products.ids).
      where("spree_option_values.id = ?", option.id).
      distinct.
      count
  end

  def display_list?(display_type)
    display_type == 'list'
  end

  def extra_params(display_type, sort, color, size)
    base_params = { display_type: display_type, sort: sort }

    if color.present?
      base_params.merge(color: color, size: nil)
    elsif size.present?
      base_params.merge(color: nil, size: size)
    else
      base_params
    end
  end
end
