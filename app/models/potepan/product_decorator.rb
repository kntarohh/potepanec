module Potepan::ProductDecorator
  def related_products(taxons)
    Spree::Product.in_taxons(taxons).
      distinct.
      includes(master: [:default_price, :images]).
      where.not(id: id)
  end

  def self.prepended(base)
    base.scope :search_products, -> (search_params) do
      return if search_params.blank?

      includes(master: [:default_price, :images]).
        where('name LIKE ? OR description LIKE ?', "%#{search_params}%", "%#{search_params}%").
        distinct
    end
  end

  Spree::Product.prepend self
end
