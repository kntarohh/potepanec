require 'rails_helper'

RSpec.describe 'Categories', type: :system do
  let(:taxonomy) { create(:taxonomy, name: 'Category') }
  let(:taxonomy_taxon) { taxonomy.root.children.create(name: 'Category') }
  let(:taxon) { create(:taxon, name: 'Bag', taxonomy: taxonomy, parent_id: taxonomy_taxon.id) }
  let!(:product1) { create(:product, name: 'Tote', price: '111', taxons: [taxon], available_on: "Mon, 21 Oct 2019 02:25:06 JST +09:00") } # rubocop:disable Metrics/LineLength
  let!(:product2) { create(:product, name: 'Pouch', price: '222', taxons: [taxon], available_on: "Mon, 21 Oct 2019 02:25:05 JST +09:00") } # rubocop:disable Metrics/LineLength
  let!(:product3) { create(:product, name: 'Back-Pack', price: '444', taxons: [taxon], available_on: "Mon, 21 Oct 2019 02:25:04 JST +09:00") } # rubocop:disable Metrics/LineLength
  let!(:product4) { create(:product, name: 'Boston-Bag', price: '333', taxons: [taxon], available_on: "Mon, 21 Oct 2019 02:25:03 JST +09:00") } # rubocop:disable Metrics/LineLength
  let!(:product5) { create(:product, name: 'Shoulder-Bag', price: '555', taxons: [taxon], available_on: "Mon, 21 Oct 2019 02:25:02 JST +09:00") } # rubocop:disable Metrics/LineLength
  let!(:product6) { create(:product, name: 'Messenger-Bag', price: '666', taxons: [taxon], available_on: "Mon, 21 Oct 2019 02:25:01 JST +09:00") } # rubocop:disable Metrics/LineLength
  let!(:variant1) { create(:variant, product_id: product1.id, option_values: [option_value1, option_value4]) } # rubocop:disable Metrics/LineLength
  let!(:variant2) { create(:variant, product_id: product1.id, option_values: [option_value2, option_value5]) } # rubocop:disable Metrics/LineLength
  let!(:variant3) { create(:variant, product_id: product1.id, option_values: [option_value3, option_value6]) } # rubocop:disable Metrics/LineLength
  let!(:variant4) { create(:variant, product_id: product2.id, option_values: [option_value1]) }
  let!(:variant5) { create(:variant, product_id: product3.id, option_values: [option_value1]) }
  let!(:variant6) { create(:variant, product_id: product4.id, option_values: [option_value1]) }
  let!(:option_type1) { create(:option_type, name: 'Color', presentation: 'Color') }
  let!(:option_type2) { create(:option_type, name: 'Size', presentation: 'Size') }
  let!(:option_value1) { create(:option_value, name: 'Red', presentation: 'Red', option_type: option_type1) } # rubocop:disable Metrics/LineLength
  let!(:option_value2) { create(:option_value, name: 'Blue', presentation: 'Blue', option_type: option_type1) } # rubocop:disable Metrics/LineLength
  let!(:option_value3) { create(:option_value, name: 'Green', presentation: 'Green', option_type: option_type1) } # rubocop:disable Metrics/LineLength
  let!(:option_value4) { create(:option_value, name: 'Small', presentation: 'S', option_type: option_type2) } # rubocop:disable Metrics/LineLength
  let!(:option_value5) { create(:option_value, name: 'Miduim', presentation: 'M', option_type: option_type2) } # rubocop:disable Metrics/LineLength
  let!(:option_value6) { create(:option_value, name: 'Large', presentation: 'L', option_type: option_type2) } # rubocop:disable Metrics/LineLength

  scenario 'visit category_path simply' do
    visit potepan_category_path(taxon.id)

    within("div#product_category") do
      expect(page).to have_content('Category')
      click_link('Category')
      expect(page).to have_content('Bag (6)')
      click_link('Bag (6)')
      expect(page).not_to have_content('Bag (6)')
    end

    within("div#product_list") do
      expect(page).to have_content('TOTE')
      expect(page).to have_content('111')
      expect(page).to have_link(href: potepan_product_path(product1))
    end
  end

  scenario 'visit category_path when product has colors and sizes' do
    visit potepan_category_path(taxon.id)

    within("div#colors") do
      expect(page).to have_content('Red(4)')
      expect(page).to have_content('Blue(1)')
      expect(page).to have_content('Green(1)')
    end

    within("div#sizes") do
      expect(page).to have_content('S(1)')
      expect(page).to have_content('M(1)')
      expect(page).to have_content('L(1)')
    end

    within("div#product_list") do
      expect(page).to have_content('TOTE')
      expect(page).to have_content('111')
      expect(page).to have_link(href: potepan_product_path(product1))
    end
  end

  scenario 'visit category_path and search by Colors' do
    visit potepan_category_path(taxon.id, color: option_value1)

    within("div#product_list") do
      expect(page).to have_content('TOTE')
      expect(page).to have_content('POUCH')
      expect(page).to have_content('BACK-PACK')
      expect(page).to have_content('BOSTON-BAG')
      expect(page).not_to have_content('SHOULDER-BAG')
    end
  end

  scenario 'visit category_path and search by Sizes' do
    visit potepan_category_path(taxon.id, size: option_value4)

    within("div#product_list") do
      expect(page).to have_content('TOTE')
      expect(page).not_to have_content('POUCH')
    end
  end

  scenario 'sort by default' do
    visit potepan_category_path(taxon.id)

    within("div#product_list") do
      item_titles = all('.productCaption').map(&:text)
      expect(item_titles).to eq %W(
        TOTE\n$111.00
        POUCH\n$222.00
        BACK-PACK\n$444.00
        BOSTON-BAG\n$333.00
        SHOULDER-BAG\n$555.00
        MESSENGER-BAG\n$666.00
      )
    end
  end

  scenario 'sort by old' do
    visit potepan_category_path(taxon.id, sort: 'old')

    within("div#product_list") do
      item_titles = all('.productCaption').map(&:text)
      expect(item_titles).to eq %W(
        MESSENGER-BAG\n$666.00
        SHOULDER-BAG\n$555.00
        BOSTON-BAG\n$333.00
        BACK-PACK\n$444.00
        POUCH\n$222.00
        TOTE\n$111.00
      )
    end
  end

  scenario 'sort by low' do
    visit potepan_category_path(taxon.id, sort: 'low')

    within("div#product_list") do
      item_titles = all('.productCaption').map(&:text)
      expect(item_titles).to eq %W(
        TOTE\n$111.00
        POUCH\n$222.00
        BOSTON-BAG\n$333.00
        BACK-PACK\n$444.00
        SHOULDER-BAG\n$555.00
        MESSENGER-BAG\n$666.00
      )
    end
  end

  scenario 'sort by high' do
    visit potepan_category_path(taxon.id, sort: 'high')

    within("div#product_list") do
      item_titles = all('.productCaption').map(&:text)
      expect(item_titles).to eq %W(
        MESSENGER-BAG\n$666.00
        SHOULDER-BAG\n$555.00
        BACK-PACK\n$444.00
        BOSTON-BAG\n$333.00
        POUCH\n$222.00
        TOTE\n$111.00
      )
    end
  end
end
