require 'rails_helper'

RSpec.describe 'Search', type: :system do
  let!(:product1) { create(:product, name: 'Tote', description: 'this is a big tote bag.') } # rubocop:disable Metrics/LineLength
  let!(:product2) { create(:product, name: 'Pouch', description: 'this is a small pouch.') } # rubocop:disable Metrics/LineLength
  let!(:product3) { create(:product, name: 'Back-Pack', description: 'this is a big back pack.') } # rubocop:disable Metrics/LineLength
  let!(:product4) { create(:product, name: 'Boston-Bag', description: 'this is a small boston bag.') } # rubocop:disable Metrics/LineLength
  let!(:product5) { create(:product, name: 'Shoulder-Bag', description: 'this is a big shoulder bag.') } # rubocop:disable Metrics/LineLength
  let!(:product6) { create(:product, name: 'Messenger-Bag', description: 'this is a small messenger bag.') } # rubocop:disable Metrics/LineLength

  scenario 'search by name' do
    visit potepan_search_path(search: 'Tote')

    within("div#product_list") do
      expect(page).to have_content('TOTE')
      expect(page).not_to have_content('POUCH')
      expect(page).not_to have_content('BACK-PACK')
      expect(page).not_to have_content('BOSTON-BAG')
      expect(page).not_to have_content('SHOULDER-BAG')
      expect(page).not_to have_content('MESSENGER-BAG')
    end
  end

  scenario 'search by description' do
    visit potepan_search_path(search: 'big')

    within("div#product_list") do
      expect(page).to have_content('TOTE')
      expect(page).to have_content('BACK-PACK')
      expect(page).to have_content('SHOULDER-BAG')
      expect(page).not_to have_content('POUCH')
      expect(page).not_to have_content('BOSTON-BAG')
      expect(page).not_to have_content('MESSENGER-BAG')
    end
  end
end
