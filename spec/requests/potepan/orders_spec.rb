require 'rails_helper'

RSpec.describe "Orders", type: :request do
  let(:base_title) { "Potepanec" }
  let!(:product1) { create(:product, name: 'Tote', price: '111') }
  let!(:product2) { create(:product, name: 'Pouch', price: '222') }
  let!(:option_type1) { create(:option_type, name: 'color', presentation: 'Color') }
  let!(:option_value1) { create(:option_value, name: 'red', presentation: 'Red', option_type: option_type1) } # rubocop:disable Metrics/LineLength
  let!(:option_value2) { create(:option_value, name: 'blue', presentation: 'Blue', option_type: option_type1) } # rubocop:disable Metrics/LineLength
  let!(:variant1) { create(:variant, product: product1, price: '111', option_values: [option_value1]) } # rubocop:disable Metrics/LineLength
  let!(:variant2) { create(:variant, product: product1, price: '111', option_values: [option_value2]) } # rubocop:disable Metrics/LineLength
  let(:body) { response.body }
  let!(:store) { create(:store) }

  describe "GET /populate" do
    before do
      get populate_potepan_orders_path({ order: { variant_id: variant1.id, quantity: 3 } })
      get potepan_cart_url
    end

    it 'has title "Cart | Potepanec"' do
      expect(body).to include("Cart | #{base_title}")
    end

    it 'includes line_item.product.name' do
      expect(body).to include('Tote')
    end

    it 'includes line_item.product.display_price' do
      expect(body).to include('111')
    end

    it 'includes line_item.quantity' do
      expect(body).to include('value="3"')
    end

    it 'doesnt include items not ordered' do
      expect(body).not_to include('Pouch')
    end
  end
end
