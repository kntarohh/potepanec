require 'rails_helper'

RSpec.describe "StaticPages", type: :request do
  let(:base_title) { "Potepanec" }
  let(:body) { response.body }
  let!(:taxon1) { create(:taxon, name: 'Shirts') } # コントローラー呼び出しの際に失敗するため記述
  let!(:taxon2) { create(:taxon, name: 'Bags') } # コントローラー呼び出しの際に失敗するため記述
  let!(:taxon3) { create(:taxon, name: 'denim') } # コントローラー呼び出しの際に失敗するため記述

  context "there are new_products" do
    let!(:product1) { create(:product, name: 'Tote', price: '111', available_on: Time.now) }
    let!(:product2) { create(:product, name: 'Pouch', price: '222', available_on: Time.now) }
    let!(:product3) { create(:product, name: 'Back-Pack', price: '333', available_on: Time.now + 1) } # rubocop:disable Metrics/LineLength
    let!(:product4) { create(:product, name: 'Boston-Bag', price: '444', available_on: Time.now + 1) } # rubocop:disable Metrics/LineLength
    let!(:product5) { create(:product, name: 'Shoulder-Bag', price: '555', available_on: Time.now + 1) } # rubocop:disable Metrics/LineLength
    let!(:product6) { create(:product, name: 'Messenger-Bag', price: '666', available_on: Time.now + 1) } # rubocop:disable Metrics/LineLength
    let!(:product7) { create(:product, name: 'Blouson', price: '777', available_on: Time.now + 1) } # rubocop:disable Metrics/LineLength
    let!(:product8) { create(:product, name: 'Sneaker', price: '888', available_on: Time.now + 1) } # rubocop:disable Metrics/LineLength

    describe "basic infomation" do
      before do
        get '/potepan'
      end

      it 'has title "Home | Potepanec"' do
        expect(body).to include("Home | #{base_title}")
      end
      it 'includes 人気カテゴリー' do
        expect(body).to include('人気カテゴリー')
      end
      it 'includes 新着商品' do
        expect(body).to include('新着商品')
      end
    end

    describe "new arrivals" do
      before do
        get '/potepan'
      end

      it 'includes new products' do
        expect(body).to include('Sneaker')
        expect(body).to include('Blouson')
        expect(body).to include('Messenger-Bag')
        expect(body).to include('Shoulder-Bag')
        expect(body).to include('Boston-Bag')
        expect(body).to include('Back-Pack')
      end
      it 'doesnt include products over MAX_NUMBER_OF_NEW_PRODUCTS' do
        expect(body).not_to include('Tote')
        expect(body).not_to include('Pouch')
      end
      it 'includes link_to product' do
        expect(body).to include(potepan_product_path(product8))
      end
    end
  end

  context "there are not any new_products" do
    let!(:product9) { create(:product, name: 'Blouson', price: '777', available_on: 1.month.ago) }
    let!(:product10) { create(:product, name: 'Sneaker', price: '888', available_on: 1.month.ago) }

    describe "new arrivals" do
      before do
        get '/potepan'
      end

      it 'doesnt include products' do
        expect(body).not_to include('Blouson')
        expect(body).not_to include('Sneaker')
      end
      it 'includes 今月の新着商品はありません' do
        expect(body).to include("今月の新着商品はありません")
      end
    end
  end
end
