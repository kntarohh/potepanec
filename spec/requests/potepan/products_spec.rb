require 'rails_helper'

RSpec.describe "Products", type: :request do
  let(:base_title) { "Potepanec" }
  let(:taxonomy) { create(:taxonomy, name: 'Category') }
  let(:taxonomy_taxon) { taxonomy.root.children.create(name: 'Category') }
  let!(:taxon1) { create(:taxon, name: 'Bag', taxonomy: taxonomy, parent_id: taxonomy_taxon.id) }
  let!(:taxon2) { create(:taxon, name: 'Jacket', taxonomy: taxonomy, parent_id: taxonomy_taxon.id) }
  let!(:product1) { create(:product, name: 'Tote', price: '111', taxons: [taxon1]) }
  let!(:product2) { create(:product, name: 'Pouch', price: '222', taxons: [taxon1]) }
  let!(:product3) { create(:product, name: 'Back-Pack', price: '333', taxons: [taxon1]) }
  let!(:product4) { create(:product, name: 'Boston-Bag', price: '444', taxons: [taxon1]) }
  let!(:product5) { create(:product, name: 'Shoulder-Bag', price: '555', taxons: [taxon1]) }
  let!(:product6) { create(:product, name: 'Messenger-Bag', price: '666', taxons: [taxon1]) }
  let!(:product7) { create(:product, name: 'Blouson', price: '777', taxons: [taxon2]) }
  let!(:product8) { create(:product, name: 'Sneaker', price: '888') }
  let(:body) { response.body }

  describe "basic infomation" do
    before do
      get potepan_product_path(product1.id)
    end

    it 'has title "Product | Potepanec"' do
      expect(body).to include("Product | #{base_title}")
    end

    it 'includes @product.name' do
      expect(body).to include('Tote')
    end

    it 'includes @product.display_price' do
      expect(body).to include('111')
    end
  end

  describe "related products" do
    context "product has taxon" do
      before do
        get potepan_product_path(product1.id)
      end

      it 'includes related products' do
        expect(body).to include('Pouch')
        expect(body).to include('Back-Pack')
        expect(body).to include('Boston-Bag')
        expect(body).to include('Shoulder-Bag')
      end

      it 'doesnt include products over MAX_NUMBER_OF_RELATED_PRODUCTS' do
        expect(body).not_to include('Messenger-Bag')
      end

      it 'doesnt include nonrelated products' do
        expect(body).not_to include('Blouson')
      end

      it 'includes link_to categories' do
        expect(body).to include(potepan_category_path(taxon1.id))
      end
    end

    context "product has no taxon" do
      before do
        get potepan_product_path(product8.id)
      end

      it 'doesnt include any related products' do
        expect(body).not_to include('Tote')
        expect(body).not_to include('Pouch')
        expect(body).not_to include('Back-Pack')
        expect(body).not_to include('Blouson')
      end

      it 'doesnt include link_to categories' do
        expect(body).not_to include(potepan_category_path(taxon1.id))
        expect(body).not_to include(potepan_category_path(taxon2.id))
      end
    end
  end
end
