require 'rails_helper'

RSpec.describe "CategoriesRequests", type: :request do
  let(:taxonomy) { create(:taxonomy, name: 'Category') }
  let(:taxonomy_taxon) { taxonomy.root.children.create(name: 'Category') }
  let(:taxon) { create(:taxon, name: 'Bag', taxonomy: taxonomy, parent_id: taxonomy_taxon.id) }
  let!(:product1) { create(:product, name: 'Tote', price: '123', taxons: [taxon]) }
  let!(:product2) { create(:product, name: 'Mug') }
  let!(:option_type1) { create(:option_type, name: 'Color', presentation: 'Color') } # コントローラー呼び出しで失敗するため記述　# rubocop:disable Metrics/LineLength
  let!(:option_type2) { create(:option_type, name: 'Size', presentation: 'Size') } # コントローラー呼び出しで失敗するため記述　# rubocop:disable Metrics/LineLength
  let(:base_title) { "Potepanec" }
  let(:body) { response.body }

  describe "GET /categories" do
    context 'taxon has no descendant' do
      before do
        get potepan_category_path(taxon.id)
      end

      it 'has title "Categories | Potepanec"' do
        expect(body).to include("#{taxon.name} | #{base_title}")
      end

      it "includes taxonomy.name" do
        expect(body).to include('Category')
      end

      it "includes taxon.name" do
        expect(body).to include('Bag')
      end

      it "includes product.name & product.display_price" do
        expect(body).to include('Tote')
        expect(body).to include('123')
      end

      it "doesn't include product.name without taxons" do
        expect(body).not_to include('Mug')
      end
    end

    context 'taxon has descendant' do
      before do
        get potepan_category_path(taxonomy_taxon.id)
      end

      it "includes product.name & product.display_price" do
        expect(body).to include('Tote')
        expect(body).to include('123')
      end

      it "doesn't include product.name without taxons" do
        expect(body).not_to include('Mug')
      end
    end
  end
end
